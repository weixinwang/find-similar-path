function [ grav,gyroBias ] = getGravity( gyro, acce )
% This function uses the magnitude of acceleration during stationary periods
% as gravity. This is questionable to me now, maybe it's better to just use
% a fixed gravity such as 9.8. This function also finds out the gyroscope
% bias during stationary periods.
% Last edited by Weixin Wang, January 3, 2019

% find low-motion periods
lowerThreshold = 0.05;
timeThreshold = 20*1000;

gyroMag = sqrt(sum(gyro.^2,2));
lowGyroStart = find(diff(gyroMag<lowerThreshold) == 1)+1;
lowGyroEnd = find(diff(gyroMag<lowerThreshold) == -1);
if gyroMag(1)<lowerThreshold
    lowGyroStart = [1;lowGyroStart];
end
if gyroMag(end)<lowerThreshold
    lowGyroEnd = [lowGyroEnd;length(gyro)];
end

% The stationary period must be very long to get gravity and gyroscope bias
% estimations
lowGyroLength = lowGyroEnd-lowGyroStart;
lowGyroStart(lowGyroLength<timeThreshold) = [];
lowGyroEnd(lowGyroLength<timeThreshold) = [];

% calculate bias
if isempty(lowGyroStart)
    warning('no low angular velocity period found, gyro bias is set to 0');
    gyroBias = [0,0,0];
else
    lowGyroIndex = false(length(gyro),1);
    for i = 1:length(lowGyroStart)
        lowGyroIndex(lowGyroStart(i):lowGyroEnd(i)) = true;
    end
    gyroBias = mean(gyro(lowGyroIndex,:),1);
end

% calculate gravity
grav = mean(sqrt(sum(acce(lowGyroIndex,:).^2,2)));

% plot
% figure; hold on;
% plot(gyro);
% plot(gyro-bias);

end

