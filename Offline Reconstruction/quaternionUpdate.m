function [ newQuaternion, newPKF ] = quaternionUpdate( oldQuaternion, gyro, acce, needCorrection, PKF, QKF, RKF )
% quaternion integration and variance propagation
% Note: this algorithm was originally suggested by Peter Adamczyk from a
% patent: US6061611A Closed-form integrator for the quaternion (euler angle) kinematics equations.
% It can be shown that this algorithm is exactly the zeorth order
% integration formula in Joan Sola, Quaternion kinametics for the error-state Kalman Filter, 2017
% Note2: this variance propagation comes from the code by Lauro Ojeda

global tdelta;

if needCorrection == 0 % Do not apply correction algorithm
    % uncertainty propagation
    newPKF = PKF+QKF;
    
    % quaternion integration
    if sqrt(gyro(1)^2+gyro(2)^2+gyro(3)^2) ~= 0
        TIARM = sqrt(gyro(1)^2+gyro(2)^2+gyro(3)^2)*tdelta;
    else
        newQuaternion = oldQuaternion;
        newPKF = PKF;
        return
    end
    OMEGA = [0 -gyro(1) -gyro(2) -gyro(3)
             gyro(1) 0 gyro(3) -gyro(2)
             gyro(2) -gyro(3) 0 gyro(1)
             gyro(3) gyro(2) -gyro(1) 0]*tdelta./2;
    STM = cos(TIARM/2)*eye(4)+2/TIARM*sin(TIARM/2)*OMEGA;
    newQuaternion = (STM*oldQuaternion')';
end

if needCorrection == 1 % Apply correction algorithm
    % uncertainty propagation
    PKF = PKF+QKF;
    
    % quaternion integration
    if sqrt(gyro(1)^2+gyro(2)^2+gyro(3)^2) ~= 0
        TIARM = sqrt(gyro(1)^2+gyro(2)^2+gyro(3)^2)*tdelta;
    else
        newQuaternion = oldQuaternion;
        newPKF = PKF;
        return
    end
    OMEGA = [0 -gyro(1) -gyro(2) -gyro(3)
             gyro(1) 0 gyro(3) -gyro(2)
             gyro(2) -gyro(3) 0 gyro(1)
             gyro(3) gyro(2) -gyro(1) 0]*tdelta./2;
    STM = cos(TIARM/2)*eye(4)+2/TIARM*sin(TIARM/2)*OMEGA;
    newQuaternion = (STM*oldQuaternion')';
    
    % Kalman filter update
    K = PKF/(PKF+RKF);
    newQuaternion = getCorrection(newQuaternion,acce',K);
    newPKF = (1-K)*PKF*(1-K)+K*RKF*K;
end

end
