function [ Q0 ] = getInitQuat( acce )
% This is an old version function of converting acceleration to orientation
% by assuming no free acceleration.

% first compute the rotation matrix
grav = sqrt(acce(1)*acce(1) + acce(2)*acce(2) + acce(3)*acce(3));
zx = -acce(1)/grav;
zy = -acce(2)/grav;
zz = -acce(3)/grav;

if zx ~= 0
    xx = 0;
    xy = sqrt(1/(1+zy^2/zz^2));
    xz = -xy*zy/zz;
elseif zy ~= 0
    xy = 0;
    xx = sqrt(1/(1+zx^2/zz^2));
    xz = -xx*zx/zz;
else
    xz = 0;
    xx = sqrt(1/(1+zx^2/zy^2));
    xy = -xx*zx/zy;
end

y = cross([zx,zy,zz],[xx,xy,xz]);


R0(1,:) = [xx,xy,xz];
R0(2,:) = y;
R0(3,:) = [zx,zy,zz];

% convert the rotation matrix to quaternion
t = R0(1,1)+R0(2,2)+R0(3,3);
if t>=0
    r = sqrt(1+t);
    s = 0.5/r;
    w = 0.5*r;
    x = (R0(3,2)-R0(2,3))*s;
    y = (R0(1,3)-R0(3,1))*s;
    z = (R0(2,1)-R0(1,2))*s;
elseif max([R0(1,1),R0(2,2),R0(3,3)])==R0(1,1)
    r = sqrt(1+R0(1,1)-R0(2,2)-R0(3,3));
    s = 0.5/r;
    w = (R0(3,2)-R0(2,3))*s;
    x = 0.5*r;
    y = (R0(1,2)+R0(2,1))*s;
    z = (R0(3,1)+R0(1,3))*s;
elseif max([R0(1,1),R0(2,2),R0(3,3)])==R0(2,2)
    r = sqrt(1+R0(2,2)-R0(1,1)-R0(3,3));
    s = 0.5/r;
    w = (R0(1,3)-R0(3,1))*s;
    x = (R0(1,2)+R0(2,1))*s;
    y = 0.5*r;
    z = (R0(3,2)+R0(2,3))*s;
elseif max([R0(1,1),R0(2,2),R0(3,3)])==R0(3,3)
    r = sqrt(1+R0(3,3)-R0(1,1)-R0(2,2));
    s = 0.5/r;
    w = (R0(2,1)-R0(1,2))*s;
    x = (R0(1,3)+R0(3,1))*s;
    y = (R0(2,3)+R0(3,2))*s;
    z = 0.5*r;
end
Q0 = [w;x;y;z];

end