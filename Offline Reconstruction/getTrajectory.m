function [ position, strideLength, strideWidth, clearance, velocity, numStep ] = getTrajectory( gyro,acce,gyroBias,acceBias,grav )
% Note: this function is an old but verified version of trajectory
% reconstruction from IMU. The whole idea was credited to Lauro Ojeda at
% University of Michigan, https://me.engin.umich.edu/people/faculty/lauro-ojeda
% Reference: John R. Rebula, Measurement of foot placement and its variability with inertial sensors, 2013

%% Calibration
% the bias data come from the estimate from the Kalman filter in the main reconstruction code
gyro = gyro-gyroBias;
acce = acce-acceBias;

%% constants and initialization
% sample period
global tdelta;
tdelta = 0.005;

% thresholds
gyroThresholdVelocity = 1;
gyroThresholdOrientation = 0.8;
acceThreshold = 0.8;
stanceThreshold = 5;
swingThreshold = 5;

% true means swing, false means swing
walkStatus = false(length(gyro),1);

% quaternion vector
quaternion = zeros(length(gyro),4);

% acceleration, velocity and position in world frame vector
acceleration = zeros(length(gyro),3);
velocity = zeros(length(gyro),3);
position = zeros(length(gyro),3);

% initial quaternion is determined using the first accelerometer data
quaternion(1,:) = getInitQuat(acce(1,:));

% initial kalman filter parameters
PKF = 0; QKF = 1.5e-5*tdelta; RKF = 1.5e-1*tdelta;

%% Stride segmentation
% find stance and swing
for i = 1:1:length(gyro)-swingThreshold
    if ~walkStatus(i)
        % comparing next swingThreshold samples to determine whether becomes swing
        N1 = find(sqrt(sum(gyro(i+1:i+swingThreshold,:).^2,2))>gyroThresholdVelocity);
        N2 = find(abs(sqrt(sum(acce(i+1:i+swingThreshold,:).^2,2))-grav)>acceThreshold);
        if size(N1,1) == swingThreshold || size(N2,1) == swingThreshold
            walkStatus(i+1) = true;
        else
            walkStatus(i+1) = false;
        end
    else
        % comparing next stanceThreshold samples to determine whether becomes stance
        N1 = find(sqrt(sum(gyro(i+1:i+stanceThreshold,:).^2,2))<gyroThresholdVelocity);
        N2 = find(sqrt(sum(acce(i+1:i+stanceThreshold,:).^2,2))-grav<acceThreshold);
        if size(N1,1) == stanceThreshold && size(N2,1) == stanceThreshold
            walkStatus(i+1) = false;
        else
            walkStatus(i+1) = true;
        end
    end
end

% find heelStrikeIndex, toeOffIndex
toeOffIndex = find(diff(walkStatus) == 1);
heelStrikeIndex = find(diff(walkStatus) == -1);

% smoothing by deleting the short time gap
shortIndex = find(toeOffIndex(2:end)-heelStrikeIndex(1:end-1) < 10);
toeOffIndex(shortIndex+1) = [];
heelStrikeIndex(shortIndex) = [];

shortIndex = find(heelStrikeIndex-toeOffIndex < 10);
toeOffIndex(shortIndex) = [];
heelStrikeIndex(shortIndex) = [];

if toeOffIndex(1)<heelStrikeIndex(1)
    heelStrikeIndex = [1;heelStrikeIndex];
end
if toeOffIndex(end)<heelStrikeIndex(end)
    toeOffIndex = [toeOffIndex;length(gyro)];
end

% find midStanceIndex
midStanceIndex = [];
dummyThreshold = 200;
for i = 1:1:size(heelStrikeIndex)
    if toeOffIndex(i)-heelStrikeIndex(i)<dummyThreshold
        % use the time when the magnitude of gyro is the smallest as mid-stance
        [minGyro,midIndex] = min(sum(gyro(heelStrikeIndex(i):toeOffIndex(i),:).^2,2));
        midIndex = midIndex+heelStrikeIndex(i);
    else  % add dummy midStanceIndex to restrict drift during long stance phase
        midIndex = heelStrikeIndex(i)+50:100:toeOffIndex(i)-50;
    end
    midStanceIndex = [midStanceIndex,midIndex];
end

%% forward integration
for i = 2:length(gyro)
    % integrate and update quaternion
    applyCorrection = norm(gyro(i,:))<gyroThresholdOrientation && abs(norm(acce(i,:))-grav)<acceThreshold;
    [quaternion(i,:),PKF] = quaternionUpdate(quaternion(i-1,:),gyro(i,:),acce(i,:),applyCorrection,PKF,QKF,RKF);
    
    % use quaternion to calculate acceleration in world frame
    acceleration(i,:) = -(QuatToRM(quaternion(i,:))*acce(i,:)')'-[0 0 grav];
    
    % velocity
    velocity(i,:) = velocity(i-1,:)+acceleration(i,:)*tdelta;
    
    % position
    position(i,:) = position(i-1,:)+velocity(i,:)*tdelta+0.5*acceleration(i,:)*tdelta^2;
    
    % velocity and position correction at midstance
    if ~isempty(find(midStanceIndex==i,1))
        % back correct position
        if find(midStanceIndex==i) ~= 1
            preIndex = midStanceIndex(find(midStanceIndex==i)-1);
            acceError = velocity(i,:)/((i-preIndex)*tdelta);
            for j = preIndex:1:i
                position(j,:) = position(j,:)-0.5*acceError*((j-preIndex)*tdelta)^2;
            end
        end
        
        % velocity set to zero
        velocity(i,:) = [0,0,0];
    end
end

%% compute gait parameters
% delete the first and last steps
midStanceIndex([1,end]) = [];
numStep = length(midStanceIndex);

% stride length and stride width and velocity
alpha = atan2(diff(position(midStanceIndex,2)),diff(position(midStanceIndex,1)));
strideDistance = zeros(length(midStanceIndex)-1,1);
velocity = zeros(length(midStanceIndex)-1,1);
strideLength = zeros(length(midStanceIndex)-1,1);
strideWidth = zeros(length(midStanceIndex)-1,1);
if length(midStanceIndex)<7
    % if the length of the walking session is too short, return nothing
    position = [];
    strideLength = [];
    strideWidth = [];
    clearance = [];
    velocity = [];
    warning('too few steps found in the trial, empty sets are returned');
    return;
else
    for i = 1:length(midStanceIndex)-1
        % define walking direction as the major priciple axis of the
        % covariance matrix of 7 mid-stance locations
        if i < 4
            theta = covfit(position(midStanceIndex(1:7),1),position(midStanceIndex(1:7),2));
        elseif i > length(midStanceIndex)-3
            theta = covfit(position(midStanceIndex(end-6:end),1),position(midStanceIndex(end-6:end),2));
        else
            theta = covfit(position(midStanceIndex(i-3:i+3),1),position(midStanceIndex(i-3:i+3),2));     
        end
        strideDistance(i) = norm(position(midStanceIndex(i+1),:)-position(midStanceIndex(i),:));
        velocity(i) = strideDistance(i)/((midStanceIndex(i+1)-midStanceIndex(i))*tdelta);
        % stride length and width are defined as the projection of the step
        % trajectory onto walking direction and its perpendicular direction
        strideLength(i) = strideDistance(i)*abs(cos(theta-alpha(i)));
        strideWidth(i) = strideDistance(i)*abs(sin(theta-alpha(i)));
    end
end

% virtual clearance
height = diff(position(midStanceIndex,3));
clearance = zeros(length(midStanceIndex)-1,1);
for i = length(midStanceIndex)-1:-1:1
    % a linear z-position correction so that the two consecutive mid-stance
    % have the same elevation
    deltaHeight = (0:(midStanceIndex(i+1)-midStanceIndex(i)))/(midStanceIndex(i+1)-midStanceIndex(i))*height(i);
    z = position(midStanceIndex(i):midStanceIndex(i+1),3)-deltaHeight';
    
    % virtual clearance is defined as the lowest z-position between the
    % first and last z-position peaks
    diffz = diff(z);
    peakIndex = find(diffz(1:end-1)>0 & diffz(2:end)<0)+1;
    [aux,I] = sort(z(peakIndex));
    if length(I)>1
        clearance(i) = min(z(min(peakIndex(I(end-1:end))):max(peakIndex(I(end-1:end)))))-z(1);
    end
end

% delete unnatural gaits
strideLength(strideDistance < 0.8 | strideDistance > 1.8) = []; % strideDistance is too long or too short
strideWidth(strideDistance < 0.8 | strideDistance > 1.8) = [];
clearance(strideDistance < 0.8 | strideDistance > 1.8) = [];
velocity(strideDistance < 0.8 | strideDistance > 1.8) = [];
strideLength(clearance <= 0) = []; % clearance is smaller than 0, weird
strideWidth(clearance <= 0) = [];
velocity(clearance <= 0) = [];
clearance(clearance <= 0) = [];
crab = strideWidth./strideLength > 0.5; % moving like a crab
strideLength(crab) = [];
strideWidth(crab) = [];
clearance(crab) = [];
velocity(crab) = [];

%% plotting
% figure;
% plot(acceleration); hold on;
% plot(walkStatus);
% a = gca;
% a.XTick = midStanceIndex;
% grid on;

% figure; hold on;
% plot(clearance);
% 
% figure;
% plot3(position(:,1),position(:,2),position(:,3));
% grid on; axis equal;

end

%% fit walking direction using covariance
function [theta] = covfit(x,y)

% eigenvalue decomposition of the covariance matrix
[V,D] = eig(cov(x,y));

if D(1,1)>D(2,2)
    theta = atan2(V(2,1),V(1,1));
else
    theta = atan2(V(2,2),V(1,2));
end

end

