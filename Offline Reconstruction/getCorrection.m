function [ Qnew ] = getCorrection( Q, gAcce, K )
% This function uses the measured acceleration and Kalman gain to correct
% the tilt angles of the IMU orientation. This is a really old version of
% algorithm coming from Lauro Ojeda

% convert acceleration measurement to tilt angle measurement
grav = 9.80297286843;

acce_theta = gAcce(1)/grav;
if acce_theta>=-1 && acce_theta<=1
    acce_theta = asin(acce_theta);
elseif acce_theta < -1
    acce_theta = -pi/2;
else
    acce_theta = pi/2;
end
acce_phi = gAcce(2)/cos(acce_theta)/grav;
if acce_phi>=-1 && acce_phi<=1
    acce_phi = -asin(acce_phi);
elseif acce_phi < -1
    acce_phi = pi/2;
else
    acce_phi = -pi/2;
end

% Kalman filtering on Euler angles, keep the heading angle uncorrected
euler = qua2eul(Q');
Qnew = eul2qua([euler(1) - (euler(1) - acce_phi)*K, euler(2) - (euler(2) - acce_theta)*K, euler(3)]);

end