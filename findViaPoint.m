function [ viaPoint, viaIndex ] = findViaPoint( gyro, acce, position )
% This function is the first step to simplify the trajectory by finding
% typical points that can represent the whole trajectory.
% Output: viaPoint->the positions of the selected points
%         viaIndex->the indices of the selected points in terms of IMU index

% thresholds
gyroThreshold = 1.0;
acceThreshold = 0.8;
highMotionThreshold = 10;
lowMotionThreshold = 10;
shortThreshold = 400;

% constants
interval = 200;

% gravity
grav = 9.8027;

% find low-motion periods
lowMotion = sqrt(sum(gyro.^2,2))<gyroThreshold & abs(sqrt(sum(acce.^2,2))-grav)<acceThreshold;
lowMotionStart = find(diff(lowMotion) == 1)+1;
lowMotionEnd = find(diff(lowMotion) == -1);
if lowMotion(1)
    lowMotionStart = [1;lowMotionStart];
end
if lowMotion(end)
    lowMotionEnd = [lowMotionEnd;length(lowMotion)];
end

% delete those short high motion periods
highMotionLength = lowMotionStart(2:end)-lowMotionEnd(1:end-1);
lowMotionStart(find(highMotionLength <= highMotionThreshold) + 1) = [];
lowMotionEnd(highMotionLength <= highMotionThreshold) = [];

% delete those short low motion periods
lowMotionLength = lowMotionEnd-lowMotionStart;
lowMotionStart(lowMotionLength <= lowMotionThreshold) = [];
lowMotionEnd(lowMotionLength <= lowMotionThreshold) = [];

% for short low-motion periods, just use the middle point as via-point
lowMotionLength = lowMotionEnd-lowMotionStart;
viaIndex = round((lowMotionStart(lowMotionLength <= shortThreshold)+lowMotionEnd(lowMotionLength <= shortThreshold))/2);

% for long low-motion periods, insert via-points every 200 samples
longLowMotionStart = lowMotionStart(lowMotionLength > shortThreshold);
longLowMotionEnd = lowMotionEnd(lowMotionLength > shortThreshold);
for i = 1:length(longLowMotionStart)
    viaIndex = [viaIndex;(longLowMotionStart(i):interval:longLowMotionEnd(i))'];
end

% for long high-moition periods, insert via-points every 200 samples
highMotionStart = lowMotionEnd(1:end-1)+1;
highMotionEnd = lowMotionStart(2:end);
highMotionLength = highMotionEnd-highMotionStart;
longHighMotionStart = highMotionStart(highMotionLength > shortThreshold);
longHighMotionEnd = highMotionEnd(highMotionLength > shortThreshold);
for i = 1:length(longHighMotionStart)
    viaIndex = [viaIndex;(longHighMotionStart(i):interval:longHighMotionEnd(i))'];
end

% put the indices in ascending sequence
viaIndex = sort(viaIndex);
viaPoint = position(viaIndex,:);

end

