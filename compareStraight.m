function [  ] = compareStraight( gyro1,acce1,position1,gyroBias1,acceBias1,gyro2,acce2,position2,gyroBias2,acceBias2 )
% This function is the entry function of this project. This function finds
% out the straight line walking paths that happened at close locations,
% computes gait parameters of these paths and store them.
% Last edited by Weixin Wang, January 4, 2019

addpath('Offline Reconstruction');

% check file path. The function will store data in a folder called
% "parameters data" in the parent dir of current dir. If there's no such
% folder, the function will create one. If the folder is not empty, the
% function will clear all files and dirs in the folder and then write.
dirName = '../parameters data';
if ~exist(dirName,'dir')
    mkdir(dirName);
end
list = dir(dirName);
if size(list,1) > 2
    % The first two are '.' and '..'
    for i = 3:size(list,1)
        fileName = strcat(dirName,'/',list(i).name);
        delete(fileName);
    end
end

% number of the trials for each condition
n1 = length(gyro1);
n2 = length(gyro2);

% group straight walking paths that happed at same (close) locations
[similarIndex1,similarIndex2] = findStraight(gyro1,acce1,position1,gyro2,acce2,position2);

% find gravity, maybe delete this part in future versions
grav1 = zeros(n1,1);
for i = 1:length(gyro1)
    grav1(i) = getGravity(gyro1{i},acce1{i});
end
grav2 = zeros(n2,1);
for i = 1:length(gyro2)
    grav2(i) = getGravity(gyro2{i},acce2{i});
end

% use old but verified algorithm to compute gait parametera
% Only the group that has paths found in more than numThreshold different sessions and has more than stepThreshold steps for each condition is reserved
numThreshold = 3;     % change this parameter!
stepThreshold = 300;
numOfCom = 0;
for i = 1:length(similarIndex1{1})
    % pre-allocate memory
    trajectory1 = cell(n1,1);
    trajectory2 = cell(n2,1);
    strideLength1 = cell(n1,1);
    strideLength2 = cell(n2,1);
    strideWidth1 = cell(n1,1);
    strideWidth2 = cell(n2,1);
    clearance1 = cell(n1,1);
    clearance2 = cell(n2,1);
    velocity1 = cell(n1,1);
    velocity2 = cell(n2,1);

    % calculate trajectory and gait parameters for each straight walking
    % segment
    for j = 1:n1
        for k = 1:length(similarIndex1{j}{i})
            interval = similarIndex1{j}{i}{k};
            [trajectory1{j}{k},strideLength1{j}{k},strideWidth1{j}{k},clearance1{j}{k},velocity1{j}{k}] = ...
                getTrajectory(gyro1{j}(interval,:),acce1{j}(interval,:),gyroBias1{j}(interval,:),acceBias1{j}(interval,:),grav1(j));
        end
    end
    for j = 1:n2
        for k = 1:length(similarIndex2{j}{i})
            interval = similarIndex2{j}{i}{k};
            [trajectory2{j}{k},strideLength2{j}{k},strideWidth2{j}{k},clearance2{j}{k},velocity2{j}{k}] = ...
                getTrajectory(gyro2{j}(interval,:),acce2{j}(interval,:),gyroBias2{j}(interval,:),acceBias2{j}(interval,:),grav2(j));
        end
    end
    
    % number of steps in each condition
    num1 = 0; num2 = 0;
    for j = 1:n1
        if ~isempty(strideLength1{j})
            num1 = num1 + sum(cellfun(@(x)size(x,1),strideLength1{j}));
        end
    end
    for j = 1:n2
        if ~isempty(strideLength2{j})
            num2 = num2 + sum(cellfun(@(x)size(x,1),strideLength2{j}));
        end
    end
    
    figure; hold on;
    for j = 1:5
        for k = 1:length(similarIndex1{j}{i})
            index = similarIndex1{j}{i}{k};
            plot3(position1{j}(index,1),position1{j}(index,2),position1{j}(index,3));
        end
        for k = 1:length(similarIndex2{j}{i})
            index = similarIndex2{j}{i}{k};
            plot3(position2{j}(index,1),position2{j}(index,2),position2{j}(index,3),'--');
        end
    end
    axis equal; grid on;
    
    % save data, only save the groups satisfying the number thresholds
    if sum(cellfun(@(x)~isempty(x),trajectory1))>=numThreshold && sum(cellfun(@(x)~isempty(x),trajectory2))>=numThreshold ...
            && num1 >= stepThreshold && num2 >= stepThreshold
        numOfCom = numOfCom+1;
        fileName = strcat(dirName,'/path',num2str(numOfCom));
        save(fileName,'velocity1','velocity2','strideLength1','strideLength2',...
            'strideWidth1','strideWidth2','clearance1','clearance2');
    end
end
rmpath('Offline Reconstruction2');

end

