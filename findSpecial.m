function [ result ] = findSpecial(  )

%% read data
addpath('..\..\data analysis\repo\GPS and phone');

date1{1} = '9-12-2017';
date1{2} = '9-13-2017';
date1{3} = '9-14-2017';
date1{4} = '9-15-2017';
date1{5} = '9-17-2017';

date2{1} = '9-23-2017';
date2{2} = '9-24-2017';
date2{3} = '9-25-2017';
date2{4} = '9-27-2017';
date2{5} = '9-28-2017';

n1 = length(date1);
position1 = cell(n1,1);
acce1 = cell(n1,1);
gyro1 = cell(n1,1);
gyroBias1 = cell(n1,1);
acceBias1 = cell(n1,1);
phoneIndoors1 = cell(n1,1);
phoneIndex1 = cell(n1,1);
for i = 1:n1
    load(strcat('D:\result_long tracking\',num2str(i),'.mat'));
    position1{i} = result.P;
    gyroBias1{i} = result.gyroBias;
    acceBias1{i} = result.acceBias;
    phoneIndoors1{i} = result.phoneIndoors;
    clear result;
    
    load(strcat('C:\Users\12855\Google Drive\sparkfun IMU\Trials\',date1{i},'\data.mat'));
    load(strcat('C:\Users\12855\Google Drive\sparkfun IMU\Trials\',date1{i},'\phone.mat'));
    gyro1{i} = gyro;
    acce1{i} = acce;
    phone = smoothGPS(phone);
    phoneIndex1{i} = syncPhone(phone,GPS,index);
    clear gyro acce pressure time index GPS phone
end

n2 = length(date2);
position2 = cell(n2,1);
acce2 = cell(n2,1);
gyro2 = cell(n2,1);
gyroBias2 = cell(n2,1);
acceBias2 = cell(n2,1);
phoneIndoors2 = cell(n2,1);
phoneIndex2 = cell(n1,1);
for i = 1:n2
    load(strcat('D:\result_long tracking\',num2str(i+n1),'.mat'));
    position2{i} = result.P;
    gyroBias2{i} = result.gyroBias;
    acceBias2{i} = result.acceBias;
    phoneIndoors2{i} = result.phoneIndoors;
    clear result;
    
    load(strcat('C:\Users\12855\Google Drive\sparkfun IMU\Trials\',date2{i},'\data.mat'));
    load(strcat('C:\Users\12855\Google Drive\sparkfun IMU\Trials\',date2{i},'\phone.mat'));
    gyro2{i} = gyro;
    acce2{i} = acce;
    phone = smoothGPS(phone);
    phoneIndex2{i} = syncPhone(phone,GPS,index);
    clear gyro acce pressure time index GPS phone
end

clear date1 date2 i n1 n2
rmpath('..\..\data analysis\repo\GPS and phone');

%% find total steps
% put the 1 and 2 together
n1 = length(gyro1);
n2 = length(gyro2);
gyro = [gyro1;gyro2]; clear gyro1 gyro2
acce = [acce1;acce2]; clear acce1 acce2
gyroBias = [gyroBias1;gyroBias2]; clear gyroBias1 gyroBias2
acceBias = [acceBias1;acceBias2]; clear acceBias1 acceBias2
position = [position1;position2]; clear position1 position2

% thresholds
gyroThreshold = 1.0;
acceThreshold = 0.8;
highMotionThreshold = 10;
lowMotionThreshold = 10;

% gravity
grav = 9.8027;

result.numTotalSteps = zeros(10,1);
for i = 1:10
    % find low-motion periods
    lowMotion = sqrt(sum(gyro{i}.^2,2))<gyroThreshold & abs(sqrt(sum(acce{i}.^2,2))-grav)<acceThreshold;
    lowMotionStart = find(diff(lowMotion) == 1)+1;
    lowMotionEnd = find(diff(lowMotion) == -1);
    if lowMotion(1)
        lowMotionStart = [1;lowMotionStart];
    end
    if lowMotion(end)
        lowMotionEnd = [lowMotionEnd;length(lowMotion)];
    end

    % delete those short high motion periods
    highMotionLength = lowMotionStart(2:end)-lowMotionEnd(1:end-1);
    lowMotionStart(find(highMotionLength <= highMotionThreshold) + 1) = [];
    lowMotionEnd(highMotionLength <= highMotionThreshold) = [];

    % delete those short low motion periods
    lowMotionLength = lowMotionEnd-lowMotionStart;
    lowMotionStart(lowMotionLength <= lowMotionThreshold) = [];
    result.numTotalSteps(i) = length(lowMotionStart);
end

clear gyroThreshold acceThreshold highMotionThreshold lowMotionThreshold
clear lowMotion lowMotionStart lowMotionEnd highMotionLength lowMotionLength i grav

%% find straight line walking
% threshold
distanceThreshold = 15;        % minimum length of straight walking
RDPThreshold = 1;              % trajectory simplification parameters
VISThreshold = 2.85;

% simplify trajectory
viaPoint = cell(n1+n2,1);
viaIndex = cell(n1+n2,1);
simplifiedViaPoint = cell(n1+n2,1);
simplifiedViaIndex = cell(n1+n2,1);
parfor i = 1:n1+n2
    % note: the consequence of this trajectory simplification is the
    % trajectory between simplifiedViaPoints is roughly straight
    [viaPoint{i},viaIndex{i}] = findViaPoint(gyro{i},acce{i},position{i});
    [simplifiedViaPoint{i},simplifiedViaIndex{i}] = RDP(viaPoint{i},viaIndex{i},RDPThreshold);
    [simplifiedViaPoint{i},simplifiedViaIndex{i}] = vis(simplifiedViaPoint{i},simplifiedViaIndex{i},VISThreshold);
end

% find straight walking by setting a distance threshold to the distance
% between two simplifiedViaPoints
straightStart = cell(n1+n2,1);
straightEnd = cell(n1+n2,1);
for i = 1:n1+n2
    distance = sqrt(sum(diff(simplifiedViaPoint{i}).^2,2));
    straightStart{i} = find(distance > distanceThreshold);
    straightEnd{i} = straightStart{i}+1;
end

% delete periods seem to be in vehicle
gyroMagThreshold = 1;
for i = 1:n1+n2
    aveGyroMag = zeros(length(straightStart{i}),1);
    for j = 1:length(straightStart{i})
        gyroMag = sqrt(sum(gyro{i}(simplifiedViaIndex{i}(straightStart{i}(j)):simplifiedViaIndex{i}(straightEnd{i}(j)),:).^2,2));
        aveGyroMag(j) = mean(gyroMag);
    end
    % if the IMU's motion is really small but is moving, we might treat it
    % as in a vehicle or in an elevator
    straightStart{i}(aveGyroMag<gyroMagThreshold) = [];
    straightEnd{i}(aveGyroMag<gyroMagThreshold) = [];
end

% clear intermediate variables
clear aveGyroMag distance distanceThreshold gyroMag gyroMagThreshold i j
clear RDPThreshold viaIndex viaPoint VISThreshold

%% calculate gait parameters
addpath('Offline Reconstruction');

% find gravity, maybe delete this part in future versions
grav = zeros(n1+n2,1);
parfor i = 1:n1+n2
    grav(i) = getGravity(gyro{i},acce{i});
end

% find the index for each straight segment
interval = cell(n1+n2,1);
for i = 1:n1+n2
    interval{i} = cell(length(straightStart{i}),1);
    for j = 1:length(straightStart{i})
        interval{i}{j} = simplifiedViaIndex{i}(straightStart{i}(j)):simplifiedViaIndex{i}(straightEnd{i}(j));
    end
end

% calculate gait parameters using established method
trajectory = cell(n1+n2,1);
strideLength = cell(n1+n2,1);
strideWidth = cell(n1+n2,1);
clearance = cell(n1+n2,1);
velocity = cell(n1+n2,1);
numStraightSteps = cell(n1+n2,1);

parfor i = 1:n1+n2
    for j = 1:length(interval{i})
        [trajectory{i}{j},strideLength{i}{j},strideWidth{i}{j},clearance{i}{j},velocity{i}{j},numStraightSteps{i}{j}] = ...
            getTrajectory(gyro{i}(interval{i}{j},:),acce{i}(interval{i}{j},:),gyroBias{i}(interval{i}{j},:),acceBias{i}(interval{i}{j},:),grav(i));
    end
end

for i = 1:n1+n2
    result.numStraightSteps(i) = sum(cell2mat(numStraightSteps{i}));
end

rmpath('Offline Reconstruction');
clear gyro acce gyroBias acceBias grav numStraightSteps i j

%% Group straight walking
diffThreshold = 0.2;           % maximum distance between two paths in one trial
diffThreshold2 = 0.2;          % maximum distance between two paths in two trials
densityNumThreshold = 1;       % least connected number to be grouped into a cluster

% calculate distance raster, i.e. the distance between any two straight lines
distance = cell(n1+n2);  % This is a n1+n2-by-n1+n2 cell matrix, only the top-right triangle is filled
for i = 1:n1+n2
    % distance in one trial
    
    % each cell is a n-by-n matrix, but only the top-right triangle is
    % filled with non-zeor numbers, n is the number of straight lines in
    % this trial
    distance{i,i} = zeros(length(straightStart{i}));
    for k = 1:length(straightStart{i})-1
        P1 = simplifiedViaPoint{i}(straightStart{i}(k),:);
        Q1 = simplifiedViaPoint{i}(straightEnd{i}(k),:);
        P2 = simplifiedViaPoint{i}(straightStart{i}(k+1:end),:);
        Q2 = simplifiedViaPoint{i}(straightEnd{i}(k+1:end),:);
        distance{i,i}(k,k+1:length(straightStart{i})) = lineDistance(P1,Q1,P2,Q2);
    end
    % distance between two trials
    for j = i+1:n1+n2
        % each cell is a n-by-m matrix, where n and m are the number of
        % straight lines in the two trial
        distance{i,j} = zeros(length(straightStart{i}),length(straightStart{j}));
        for k = 1:length(straightStart{i})
            P1 = simplifiedViaPoint{i}(straightStart{i}(k),:);
            Q1 = simplifiedViaPoint{i}(straightEnd{i}(k),:);
            P2 = simplifiedViaPoint{j}(straightStart{j},:);
            Q2 = simplifiedViaPoint{j}(straightEnd{j},:);
            distance{i,j}(k,:) = lineDistance(P1,Q1,P2,Q2);
        end
    end 
end

% group the straight lines happened at the same (close) locations into groups
groups = findDensityGroup(distance,diffThreshold,diffThreshold2,densityNumThreshold);

interIndex = cumsum(cellfun(@length,straightStart));
for i = 1:length(groups)
    for j = 1:size(groups{i},1)
        if groups{i}(j,2) > 1
            groups{i}(j,1) = groups{i}(j,1)-interIndex(groups{i}(j,2)-1);
        end
    end
end

clear diffThreshold diffThreshold2 densityNumThreshold distance interIndex;
clear i j k P1 P2 Q1 Q2

%% high repeated number of steps
numThreshold = 3;
stepThreshold = 300;

result.numRepeatedSteps = 0;
repeatedIndex = [];
for i = 1:length(groups)
    num1 = 0; num2 = 0;
    for j = 1:size(groups{i},1)
        if groups{i}(j,2) <= 5
            num1 = num1+length(strideLength{groups{i}(j,2)}{groups{i}(j,1)});
        else
            num2 = num2+length(strideLength{groups{i}(j,2)}{groups{i}(j,1)});
        end
    end
    
    numDay1 = length(unique(intersect(groups{i}(:,2),1:5)));
    numDay2 = length(unique(intersect(groups{i}(:,2),6:10)));
    
    if numDay1>=numThreshold && numDay2>=numThreshold && num1 >= stepThreshold && num2 >= stepThreshold
        result.numRepeatedSteps = result.numRepeatedSteps+num1+num2;
        repeatedIndex = [repeatedIndex,i];
    end
end

clear i j num1 num2 numDay1 numDay2 numThreshold stepThreshold

%% plot all straight walking with grouping
figure; hold on;
grouped = cat(1,groups{:});
[~,sortIndex] = sort(grouped(:,2));
grouped = grouped(sortIndex,:);
for i = 1:10
    groupedIndex = grouped(grouped(:,2)==i,1);
    unGroupedIndex = setdiff(1:length(straightStart{i}),groupedIndex);
    for j = 1:length(unGroupedIndex)
        index = interval{i}{unGroupedIndex(j)};
        if i <= 5
            plot(position{i}(index,1),position{i}(index,2),'Color','k');
        else
            plot(position{i}(index,1),position{i}(index,2),'Color','k','LineStyle','--');
        end
    end
end

for i = 1:length(groups)
    rng(i*10);
    color = rand(1,3);
    for j = 1:size(groups{i},1)
        index = interval{groups{i}(j,2)}{groups{i}(j,1)};
        if groups{i}(j,2) <= 5
            plot(position{groups{i}(j,2)}(index,1),position{groups{i}(j,2)}(index,2),'Color',color);
        else
            plot(position{groups{i}(j,2)}(index,1),position{groups{i}(j,2)}(index,2),'Color',color,'LineStyle','--');
        end
    end
end

clear grouped sortIndex groupedIndex unGroupedIndex index
clear i j color

%% statistic analysis: all straight line sporting shoes vs. sandals
addpath('figure properties');

% put all straight line data together
strideLength1 = cat(2,strideLength{1:5});
strideWidth1 = cat(2,strideWidth{1:5})';
clearance1 = cat(2,clearance{1:5})';
velocity1 = cat(2,velocity{1:5})';
strideLength2 = cat(2,strideLength{6:10})';
strideWidth2 = cat(2,strideWidth{6:10})';
clearance2 = cat(2,clearance{6:10})';
velocity2 = cat(2,velocity{6:10})';

strideLength1 = cat(1,strideLength1{:});
strideWidth1 = cat(1,strideWidth1{:});
clearance1 = cat(1,clearance1{:});
velocity1 = cat(1,velocity1{:});
strideLength2 = cat(1,strideLength2{:});
strideWidth2 = cat(1,strideWidth2{:});
clearance2 = cat(1,clearance2{:});
velocity2 = cat(1,velocity2{:});

% linear regression
mdl1 = fitlm(velocity1,strideLength1);  % linear regression
mdl2 = fitlm(velocity2,strideLength2);
slopeSig(1) = mdl1.Coefficients.pValue(2)<0.05 || mdl2.Coefficients.pValue(2)<0.05; % if either condition has a non-zero slope
xtest = linspace(min(min(velocity1),min(velocity2)),max(max(velocity1),max(velocity2)),100)'; % this line and below is for plotting
[yPre1,ci1] = predict(mdl1,xtest);
[yPre2,ci2] = predict(mdl2,xtest);
xmean = mean([velocity1;velocity2]);
f = figure; hold on; setFigure(f,3,2.2);
color = get(gca,'ColorOrder');
scatter(velocity1,strideLength1,1,'filled');
scatter(velocity2,strideLength2,1,'filled');
plot(xtest,yPre1,'Color','b');
plot(xtest,yPre2,'Color','r');
plot(xtest,ci1,'LineStyle',':','Color','b');
plot(xtest,ci2,'LineStyle',':','Color','r');
axes = gca; setAxes(axes,'Stride length [m]');
ylim = axes.YAxis.Limits;
plot([xmean;xmean],ylim,'Color','k','LineStyle','--');

mdl1 = fitlm(velocity1,strideWidth1);  % linear regression
mdl2 = fitlm(velocity2,strideWidth2);
slopeSig(2) = mdl1.Coefficients.pValue(2)<0.05 || mdl2.Coefficients.pValue(2)<0.05; % if either condition has a non-zero slope

mdl1 = fitlm(velocity1,clearance1);  % linear regression
mdl2 = fitlm(velocity2,clearance2);
slopeSig(3) = mdl1.Coefficients.pValue(2)<0.05 || mdl2.Coefficients.pValue(2)<0.05; % if either condition has a non-zero slope

% statistical comparison
group = [zeros(length(velocity1),1);ones(length(velocity2),1)];
if slopeSig(1)
    % ANCOVA analysis
    [~,atab,~,stats] = aoctool([velocity1;velocity2],[strideLength1;strideLength2],group,0.05,'','','','off');
    if atab{4,6} > 0.05
        [~,atab,~,stats] = aoctool([velocity1;velocity2],[strideLength1;strideLength2],group,0.05,'','','','off','parallel lines');
    end
    [~,m] = multcompare(stats,'Estimate','pmm','Display','off');
    sig(1) = atab{2,6};
    meanGait{1} = table(stats.pmm(1),stats.pmm(2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
    CIGait{1} = table(adjustedCI(strideLength1,stats.pmm(1),m(1,2)),adjustedCI(strideLength2,stats.pmm(2),m(2,2)),...
        'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
    stdGait{1} = table(m(1,2)*sqrt(length(strideLength1)),m(2,2)*sqrt(length(strideLength2)),...
        'VariableNames',{'sportshoes','sandals'},'RowNames',{'standard deviation'});
else
    % two sample t-test
    [~,p] = ttest2(strideLength1,strideLength2);
    sig(1) = p;
    meanGait{1} = table(mean(strideLength1),mean(strideLength2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
    CIGait(1) = table(CI(strideLength1),CI(strideLength2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
    stdGait{1} = table(std(strideLength1),std(strideLength2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'standare deviation'});
end

if slopeSig(2)
    [~,atab,~,stats] = aoctool([velocity1;velocity2],[strideWidth1;strideWidth2],group,0.05,'','','','off');
    if atab{4,6} > 0.05
        [~,atab,~,stats] = aoctool([velocity1;velocity2],[strideWidth1;strideWidth2],group,0.05,'','','','off','parallel lines');
    end
    [~,m] = multcompare(stats,'Estimate','pmm','Display','off');
    sig(2) = atab{2,6};
    meanGait{2} = table(stats.pmm(1),stats.pmm(2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
    CIGait{2} = table(adjustedCI(strideWidth1,stats.pmm(1),m(1,2)),adjustedCI(strideWidth2,stats.pmm(2),m(2,2)),...
        'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
    stdGait{2} = table(m(1,2)*sqrt(length(strideLength1)),m(2,2)*sqrt(length(strideLength2)),...
        'VariableNames',{'sportshoes','sandals'},'RowNames',{'standard deviation'});
else
    [~,p] = ttest2(strideWidth1,strideWidth2);
    sig(2) = p;
    meanGait{2} = table(mean(strideWidth1),mean(strideWidth2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
    CIGait{2} = table(CI(strideWidth1),CI(strideWidth2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
    stdGait{2} = table(std(strideWidth1),std(strideWidth2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'standare deviation'});
end

if slopeSig(3)
    [~,atab,~,stats] = aoctool([velocity1;velocity2],[clearance1;clearance2],group,0.05,'','','','off');
    if atab{4,6} > 0.05
        [~,atab,~,stats] = aoctool([velocity1;velocity2],[clearance1;clearance2],group,0.05,'','','','off','parallel lines');
    end
    [~,m] = multcompare(stats,'Estimate','pmm','Display','off');
    sig(3) = atab{2,6};
    meanGait{3} = table(stats.pmm(1),stats.pmm(2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
    CIGait{3} = table(adjustedCI(clearance1,stats.pmm(1),m(1,2)),adjustedCI(clearance2,stats.pmm(2),m(2,2)),...
        'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
    stdGait{3} = table(m(1,2)*sqrt(length(strideLength1)),m(2,2)*sqrt(length(strideLength2)),...
        'VariableNames',{'sportshoes','sandals'},'RowNames',{'standare deviation'});
else
    [~,p] = ttest2(clearance1,clearance2);
    sig(3) = p;
    meanGait{3} = table(mean(clearance1),mean(clearance2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
    CIGait{3} = table(CI(clearance1),CI(clearance2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
    stdGait{3} = table(std(strideWidth1),std(strideWidth2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'standare deviation'});
end

% plot
f = figure; setFigure(f,3.1,2.4);
subplot(1,3,1); hold on;
h = bar(1,meanGait{1}.sportshoes); h.FaceColor = color(1,:);
errorbar(1,meanGait{1}.sportshoes,meanGait{1}.sportshoes-CIGait{1}.sportshoes(1),'Color','k');
%errorbar(1,meanGait{1}.sportshoes,stdGait{1}.sportshoes,'Color','k');
h = bar(2,meanGait{1}.sandals); h.FaceColor = color(2,:);
errorbar(2,meanGait{1}.sandals,meanGait{1}.sandals-CIGait{1}.sandals(1),'Color','k');
%errorbar(2,meanGait{1}.sandals,stdGait{1}.sandals,'Color','k');
ax = gca; setAxes2(ax,1,sig(1));

subplot(1,3,2); hold on;
h = bar(1,meanGait{2}.sportshoes); h.FaceColor = color(1,:);
errorbar(1,meanGait{2}.sportshoes,meanGait{2}.sportshoes-CIGait{2}.sportshoes(1),'Color','k');
%errorbar(1,meanGait{2}.sportshoes,stdGait{2}.sportshoes,'Color','k');
h = bar(2,meanGait{2}.sandals); h.FaceColor = color(2,:);
errorbar(2,meanGait{2}.sandals,meanGait{2}.sandals-CIGait{2}.sandals(1),'Color','k');
%errorbar(2,meanGait{2}.sandals,stdGait{2}.sandals,'Color','k');
ax = gca; ax.XLim = [0,3]; ax.XTick = [];
setAxes2(ax,2,sig(2));

subplot(1,3,3); hold on;
h = bar(1,meanGait{3}.sportshoes); h.FaceColor = color(1,:);
errorbar(1,meanGait{3}.sportshoes,meanGait{3}.sportshoes-CIGait{3}.sportshoes(1),'Color','k');
%errorbar(1,meanGait{3}.sportshoes,stdGait{3}.sportshoes,'Color','k');
h = bar(2,meanGait{3}.sandals); h.FaceColor = color(2,:);
errorbar(2,meanGait{3}.sandals,meanGait{3}.sandals-CIGait{3}.sandals(1),'Color','k');
%errorbar(2,meanGait{3}.sandals,stdGait{3}.sandals,'Color','k');
ax = gca; ax.XLim = [0,3]; ax.XTick = [];
setAxes2(ax,3,sig(3));

% standard deviation for all straight paths
result.allStrightStd.sneaker.stridelength = std(strideLength1);
result.allStrightStd.sneaker.strideWidth = std(strideWidth1);
result.allStrightStd.sneaker.clearance = std(clearance1);
result.allStrightStd.sandal.stridelength = std(strideLength2);
result.allStrightStd.sandal.strideWidth = std(strideWidth2);
result.allStrightStd.sandal.clearance = std(clearance2);

meanVel = mean([velocity1;velocity2]);
nearIndex1 = abs(velocity1-meanVel) <= meanVel*0.02;
nearIndex2 = abs(velocity2-meanVel) <= meanVel*0.02;
allStraightAdjustedStd(1,1) = std(strideLength1(nearIndex1));
allStraightAdjustedStd(1,2) = std(strideLength2(nearIndex2));
allStraightAdjustedStd(2,1) = std(strideWidth1(nearIndex1));
allStraightAdjustedStd(2,2) = std(strideWidth2(nearIndex2));
allStraightAdjustedStd(3,1) = std(clearance1(nearIndex1));
allStraightAdjustedStd(3,2) = std(clearance2(nearIndex2));

rmpath('figure properties');
clear ci1 ci2 CIGait stdGait atab m mdl1 mdl2 meanGait sig slopeSig stats xmean
clear strideLength1 strideLength2 strideWidth1 strideWidth2 clearance1 clearance2 velocity1 velocity2
clear ax axes color f h ylim xtest yPre1 yPre2
clear meanVel nearIndex1 nearIndex2

%% find indoors and outdoors straight paths

% put the 1 and 2 together
phoneIndex = [phoneIndex1;phoneIndex2]; clear phoneIndex1 phoneIndex2
phoneIndoors = [phoneIndoors1;phoneIndoors2]; clear phoneIndoors1 phoneIndoors2

% 1-indoors; 2-outdoors; 3-not known
straightIndoors = cell(n1+n2,1);
for i = 1:n1+n2
    for j = 1:length(interval{i})
        [~,indexOfPhoneIndex] = intersect(phoneIndex{i},interval{i}{j});
        if isempty(indexOfPhoneIndex)
            straightIndoors{i}(j) = 3;
        elseif isempty(find(~phoneIndoors{i}(indexOfPhoneIndex),1))
            straightIndoors{i}(j) = 1;
        elseif isempty(find(phoneIndoors{i}(indexOfPhoneIndex),1))
            straightIndoors{i}(j) = 2;
        else
            straightIndoors{i}(j) = 3;
        end
    end
end

%% standard deviation for repeated paths
strideLengthRepeated1 = [];
strideWidthRepeated1 = [];
clearanceRepeated1 = [];
strideLengthRepeated2 = [];
strideWidthRepeated2 = [];
clearanceRepeated2 = [];

for i = repeatedIndex
    for j = 1:size(groups{i},1)
        if groups{i}(j,2) <= 5
            strideLengthRepeated1 = [strideLengthRepeated1;strideLength{groups{i}(j,2)}{groups{i}(j,1)}];
            strideWidthRepeated1 = [strideWidthRepeated1;strideWidth{groups{i}(j,2)}{groups{i}(j,1)}];
            clearanceRepeated1 = [clearanceRepeated1;clearance{groups{i}(j,2)}{groups{i}(j,1)}];
        else
            strideLengthRepeated2 = [strideLengthRepeated2;strideLength{groups{i}(j,2)}{groups{i}(j,1)}];
            strideWidthRepeated2 = [strideWidthRepeated2;strideWidth{groups{i}(j,2)}{groups{i}(j,1)}];
            clearanceRepeated2 = [clearanceRepeated2;clearance{groups{i}(j,2)}{groups{i}(j,1)}];
        end
    end
end

result.repeatedStd.sneaker.stridelength = std(strideLengthRepeated1);
result.repeatedStd.sneaker.strideWidth = std(strideWidthRepeated1);
result.repeatedStd.sneaker.clearance = std(clearanceRepeated1);
result.repeatedStd.sandal.stridelength = std(strideLengthRepeated2);
result.repeatedStd.sandal.strideWidth = std(strideWidthRepeated2);
result.repeatedStd.sandal.clearance = std(clearanceRepeated2);

%% standard deviation of std
repeatedStd = zeros(4,3,2);
for i = repeatedIndex
    strideLength1 = [];
    strideLength2 = [];
    strideWidth1 = [];
    strideWidth2 = [];
    clearance1 = [];
    clearance2 = [];
    velocity1 = [];
    velocity2 = [];
    for j = 1:size(groups{i},1)
        if groups{i}(j,2) <= 5
            strideLength1 = [strideLength1;strideLength{groups{i}(j,2)}{groups{i}(j,1)}];
            strideWidth1 = [strideWidth1;strideWidth{groups{i}(j,2)}{groups{i}(j,1)}];
            clearance1 = [clearance1;clearance{groups{i}(j,2)}{groups{i}(j,1)}];
            velocity1 = [velocity1;velocity{groups{i}(j,2)}{groups{i}(j,1)}];
        else
            strideLength2 = [strideLength2;strideLength{groups{i}(j,2)}{groups{i}(j,1)}];
            strideWidth2 = [strideWidth2;strideWidth{groups{i}(j,2)}{groups{i}(j,1)}];
            clearance2 = [clearance2;clearance{groups{i}(j,2)}{groups{i}(j,1)}];
            velocity2 = [velocity2;velocity{groups{i}(j,2)}{groups{i}(j,1)}];
        end
    end
    
    meanVel = mean([velocity1;velocity2]);
    nearIndex1 = abs(velocity1-meanVel) <= meanVel*0.02;
    nearIndex2 = abs(velocity2-meanVel) <= meanVel*0.02;
    
    repeatedStd(i==repeatedIndex,1,1) = std(strideLength1(nearIndex1));
    repeatedStd(i==repeatedIndex,2,1) = std(strideWidth1(nearIndex1));
    repeatedStd(i==repeatedIndex,3,1) = std(clearance1(nearIndex1));
    repeatedStd(i==repeatedIndex,1,2) = std(strideLength2(nearIndex2));
    repeatedStd(i==repeatedIndex,2,2) = std(strideWidth2(nearIndex2));
    repeatedStd(i==repeatedIndex,3,2) = std(clearance2(nearIndex2));
end

meanStd = reshape(mean(repeatedStd,1),3,2);
stdStd = reshape(std(repeatedStd,1),3,2);

% plot
addpath('figure properties');
f = figure; setFigure(f,3.1,2.2);
color = get(gca,'ColorOrder');
subplot(1,3,1); hold on;
h = bar(1,allStraightAdjustedStd(1,1)); h.FaceColor = color(1,:);
h = bar(2,meanStd(1,1)); h.FaceColor = color(2,:);
errorbar(2,meanStd(1,1),stdStd(1,1),'Color','k');
ax = gca; setAxes3(ax,1,[0,0.06]);

subplot(1,3,2); hold on;
h = bar(1,allStraightAdjustedStd(2,1)); h.FaceColor = color(1,:);
h = bar(2,meanStd(2,1)); h.FaceColor = color(2,:);
errorbar(2,meanStd(2,1),stdStd(2,1),'Color','k');
ax = gca; setAxes3(ax,2,[0,0.08]);

subplot(1,3,3); hold on;
h = bar(1,allStraightAdjustedStd(3,1)); h.FaceColor = color(1,:);
h = bar(2,meanStd(3,1)); h.FaceColor = color(2,:);
errorbar(2,meanStd(3,1),stdStd(3,1),'Color','k');
ax = gca; setAxes3(ax,3,[0,0.008]);

f = figure; setFigure(f,3.1,2.2);
subplot(1,3,1); hold on;
h = bar(1,allStraightAdjustedStd(1,2)); h.FaceColor = color(1,:);
h = bar(2,meanStd(1,2)); h.FaceColor = color(2,:);
errorbar(2,meanStd(1,2),stdStd(1,2),'Color','k');
ax = gca; setAxes3(ax,1,[0,0.06]);

subplot(1,3,2); hold on;
h = bar(1,allStraightAdjustedStd(2,2)); h.FaceColor = color(1,:);
h = bar(2,meanStd(2,2)); h.FaceColor = color(2,:);
errorbar(2,meanStd(2,2),stdStd(2,2),'Color','k');
ax = gca; setAxes3(ax,2,[0,0.08]);

subplot(1,3,3); hold on;
h = bar(1,allStraightAdjustedStd(3,2)); h.FaceColor = color(1,:);
h = bar(2,meanStd(3,2)); h.FaceColor = color(2,:);
errorbar(2,meanStd(3,2),stdStd(3,2),'Color','k');
ax = gca; setAxes3(ax,3,[0,0.008]);
rmpath('figure properties');

end




%% line distance
function [distance] = lineDistance(P1, Q1, P2, Q2)
% line distance is the average of three different distance: vertical
% distance, parallel distance and angle distance, normalized by the length
% of the shorter line

% P1, Q1 form segment1; P2, Q2 form segment2. P1, Q1 must be two points;
% P2, Q2 can be two n-by-3 matrices

% find out which line is shorter
length1 = norm(P1-Q1);
length2 = sqrt(sum((P2-Q2).^2,2));
PShort(length1 < length2,:) = P1.*ones(length(find(length1 < length2)),3);
PShort(length1 >= length2,:) = P2(length1 >= length2,:);
QShort(length1 < length2,:) = Q1.*ones(length(find(length1 < length2)),3);
QShort(length1 >= length2,:) = Q2(length1 >= length2,:);
PLong(length1 >= length2,:) = P1.*ones(length(find(length1 >= length2)),3);
PLong(length1 < length2,:) = P2(length1 < length2,:);
QLong(length1 >= length2,:) = Q1.*ones(length(find(length1 >= length2)),3);
QLong(length1 < length2,:) = Q2(length1 < length2,:);

% vertical distance is the sum of the distance of the two end points of the
% shorter line to the longer line
dv1 = verticalDistance(PShort,PLong,QLong);
dv2 = verticalDistance(QShort,PLong,QLong);
dv = dv1+dv2;

% parallel distance
dp = parallelDistance(PShort,QShort,PLong,QLong);

% angle distance
da = angleDistance(PShort,QShort,PLong,QLong);

distance = (dv+dp+da)/3./sqrt(sum((PShort-QShort).^2,2));

end


function [distance] = verticalDistance(P,lineStart,lineEnd)
% vertical distance is the distance from P to the line

n = (lineEnd-lineStart)./sqrt(sum((lineEnd-lineStart).^2,2));
v = (lineStart-P)-sum((lineStart-P).*n,2).*n;
distance = sqrt(sum(v.^2,2));

end


function [distance] = parallelDistance(P,Q,lineStart,lineEnd)
% parallelDistance is the sum of distance between the perpendicular feet of
% P, Q on the line to the starting points of the line.
% this parallelDistance seems to incorporate the direction of the two lines
% because the sequence of P and Q matters.

% find the two perpendicular feet
n = (lineEnd-lineStart)./sqrt(sum((lineEnd-lineStart).^2,2));
Pf = lineStart-sum((lineStart-P).*n,2).*n;
Qf = lineStart-sum((lineStart-Q).*n,2).*n;

% decide whether the foot point is on the segment or not
PJ = sum((Pf-lineStart).*(Pf-lineEnd),2);
QJ = sum((Qf-lineStart).*(Qf-lineEnd),2);

% distance
PfP2 = sqrt(sum((Pf-lineStart).^2,2));
% dP(PJ > 1e-10) = PfP2(PJ > 1e-10);
% dP(PJ <= 1e-10) = 0;
dP = PfP2';

QfQ2 = sqrt(sum((Qf-lineEnd).^2,2));
% dQ(QJ > 1e-10) = QfQ2(QJ > 1e-10);
% dQ(QJ <= 1e-10) = 0;
dQ = QfQ2';

distance = (dP+dQ)';
    
end


function [distance] = angleDistance(P,Q,lineStart,lineEnd)
% angleDistance is the angle between the two lines times the length of the
% short line. Angle distance incorporates the directions because if the two
% line are in opposite direction, acos will give pi, not 0.

theta = acos(sum((P-Q).*(lineStart-lineEnd),2)./(sqrt(sum((P-Q).^2,2)).*sqrt(sum((lineStart-lineEnd).^2,2))));
distance = theta.*sqrt(sum((P-Q).^2,2));

end

%% find connected paths
function [groups] = findDensityGroup(distance, distanceThreshold1, distanceThreshold2, densityNumThreshold)
% This function deals with two conditions, for example, wearing sporting
% shoes and sandles, and returns the straight paths that happened at the 
% same (close) locations.

% fill the empty blocks of the distance raster using symmetry
for i = 1:length(distance)
    distance{i,i} = distance{i,i}+distance{i,i}';
    distance(i+1:end,i) = cellfun(@(x)x',distance(i,i+1:end),'UniformOutput',false);
end

% find all connections between all of the straight lines, connection is
% defined as the two lines have distance smaller than threshold1 if they
% are in the same session, or smaller than threshold2 if they are in
% different sessions.
connections = cell(sum(cellfun(@(x)size(x,2),distance(1,:))),1); % the indices of all lines connected to the i'th line is stored in the i'th cell
interIndex = [0,cumsum(cellfun(@(x)size(x,2),distance(1,:)))];
for i = 1:length(distance)
    for j = 1:length(distance{i,i})
        selfConnection = find(distance{i,i}(j,:)>0 & distance{i,i}(j,:)<distanceThreshold1)+interIndex(i);
        interConnection = cell(length(distance),1);
        for k = [1:i-1,i+1:length(distance)]
            interConnection{k} = find(distance{i,k}(j,:)>0 & distance{i,k}(j,:)<distanceThreshold2)+interIndex(k);
        end
        connections{j+interIndex(i)} = [selfConnection,cat(2,interConnection{~cellfun('isempty',interConnection)})];
    end
end

% group the lines happened at the same (close) locations. A group is
% defined as every line in the group has a connected line in the group
groups = [];
for i = 1:length(connections)
    % if the line is already in a group, check the next one
    alreadyin = false;
    for j = 1:length(groups)
        if any(groups{j} == i)
            alreadyin = true;
            break;
        end
    end
    
    % find the connection tree
    if ~alreadyin
        if ~isempty(connections{i})
            group = [];
            group = findConnection(group,i,connections);
            group = sort(group);
            groups{end+1} = group;
        end
    end
end

% delete non-density connected lines from group. Density connected is a
% concept in graph theory, here a line is defined as density connected if
% it is connected to at least NumThreshold other lines in the group
for i = 1:length(groups)
    while true
        n = length(groups{i});
        j = 1;
        while j <= length(groups{i})
            if length(intersect(connections{groups{i}(j)},groups{i})) < densityNumThreshold
                groups{i}(j) = [];
            else
                j = j+1;
            end
        end
        if length(groups{i}) == n || isempty(groups{i})
            break;
        end
    end
end
groups(cellfun(@isempty,groups)) = [];

% categorize each straight walking into different trials. groups is a cell
% array, each cell is a n-by-2 matrix where n is the number of lines if the
% group. The first column of the array is the index of the line out of all
% lines, the second column of the array is the index of the session where
% the line is.
for i = 1:length(groups)
    index = cell(length(distance),1);
    for j = 1:length(distance)
        index{j} = find(groups{i}>interIndex(j) & groups{i}<=interIndex(j+1));
        groups{i}(index{j},2) = j;
    end
end

end


function [group] = findConnection(group,n,connection)
% This function is iterated recursively to exhaust the connection tree

for i = 1:length(connection{n})
    if ~any(group == connection{n}(i))
        group = [group;connection{n}(i)];
        group = findConnection(group,connection{n}(i),connection);
    end
end

end



%% calculating confidence interval
function [interval] = CI(x,alpha)
% calculate confidence interval

if ~exist('alpha','var')
    alpha = 0.05;
end

sem = std(x)/sqrt(length(x));
ts = tinv(1-alpha/2,length(x)-1);
err = ts*sem;

interval = [mean(x)-err;mean(x)+err];

end

function [interval] = adjustedCI(x,adjustedMean,adjustedSE,alpha)
% calculate adjusted confidence interval

if ~exist('alpha','var')
    alpha = 0.05;
end

ts = tinv(1-alpha/2,length(x)-1);
err = ts*adjustedSE;

interval = [adjustedMean-err;adjustedMean+err];

end