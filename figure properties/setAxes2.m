function [  ] = setAxes2( axes, which, p )
% Configure the axes of the figure

axes.Units = 'inch';
axes.Position(2) = 0.3;
axes.Position(3) = 0.65;
axes.Position(4) = 1.85;
switch which
    case 1
        axes.Position(1) = 0.3;
    case 2
        axes.Position(1) = 1.3;
    case 3
        axes.Position(1) = 2.35;
end

axes.FontName = 'Times';
axes.FontSize = 10;
axes.XLabel.FontSize = 10;
if p > 0.0001
    axes.XLabel.String = strcat({'\itp \rm= '},num2str(p,'%.4f'));
else
    axes.XLabel.String = strcat('\itp \rm< 0.0001');
end

axes.XLim = [0,3];
axes.XTick = [];
switch which
    case 1
        axes.YLim = [1.3,1.5];
    case 2
        axes.YLim = [0.04,0.08];
    case 3
        axes.YLim = [0.02,0.04];
end

axes.Title.FontName = 'Times';
axes.Title.FontSize = 10;
axes.Title.FontWeight = 'normal';
switch which
    case 1
        axes.Title.String = 'stride length';
    case 2
        axes.Title.String = 'stride width';
    case 3
        axes.Title.String = 'clearance';
end

end

