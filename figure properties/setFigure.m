function [  ] = setFigure( f, width, height )
% This function sets the size of the figure

f.Units = 'inch';
f.Position(3) = width;
f.Position(4) = height;

end

