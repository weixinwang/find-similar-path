function [outputArg1,outputArg2] = setAxes3( axes, which, yLim )

axes.Units = 'inch';
axes.Position(2) = 0.1;
axes.Position(3) = 0.65;
axes.Position(4) = 1.85;
switch which
    case 1
        axes.Position(1) = 0.3;
    case 2
        axes.Position(1) = 1.3;
    case 3
        axes.Position(1) = 2.35;
end

axes.FontName = 'Times';
axes.FontSize = 10;
axes.XLabel.FontSize = 10;

axes.XLim = [0,3];
axes.XTick = [];
if exist('yLim','var')
    axes.YLim = yLim;
end
axes.YAxis.Exponent = 0;
yTick = axes.YTick;
if length(yTick) > 5
    yTick = yTick(1:2:length(yTick));
end
axes.YTick = yTick;

axes.Title.FontName = 'Times';
axes.Title.FontSize = 10;
axes.Title.FontWeight = 'normal';
switch which
    case 1
        axes.Title.String = 'stride length';
    case 2
        axes.Title.String = 'stride width';
    case 3
        axes.Title.String = 'clearance';
end

end

