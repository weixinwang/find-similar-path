function [  ] = setAxes( axes,ylabel )
% Configure the axes of the figure

axes.Units = 'inch';
axes.Position(1) = 0.5;
axes.Position(2) = 0.45;
axes.Position(3) = 2.3;
axes.Position(4) = 1.65;

axes.FontName = 'Times';
axes.FontSize = 10;
axes.XLabel.String = 'Speed [m/s]';
axes.XLabel.FontSize = 10;
axes.YLabel.String = ylabel;
axes.YLabel.FontSize = 10;

end

