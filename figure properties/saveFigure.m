function [  ] = saveFigure( f, index, c, pathName )
% save figure

figName = strcat(pathName,'/',c,num2str(index));
savefig(figName);
jpgName = strcat(figName,'.jpg');
saveas(f,jpgName);

end

