function [ simplifiedViaPoint, stairPeriod ] = findStairs( gyro,acce,position )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

[viaPoint, viaIndex] = findViaPoint(gyro,acce,position);
[simplifiedViaPoint,simplifiedViaIndex] = RDP(viaPoint,viaIndex,1);
[simplifiedViaPoint,simplifiedViaIndex] = vis(simplifiedViaPoint,simplifiedViaIndex,2.8);

% find stairs through slope
vertical = abs(diff(simplifiedViaPoint(:,3)));
horizontal = sqrt(sum(diff(simplifiedViaPoint(:,1:2)).^2,2));
slope = vertical./horizontal;

slopeThreshold1 = 0.5;
slopeThreshold2 = 2;
verticalThreshold = 1;
stair = find(slope >= slopeThreshold1 & slope <= slopeThreshold2 & vertical >= verticalThreshold);

stairPeriod = false(length(gyro),1);
for i = 1:length(stair)
    stairPeriod(simplifiedViaIndex(stair(i)):simplifiedViaIndex(stair(i)+1)) = true;
end

% plot
figure;
plot3(simplifiedViaPoint(:,1),simplifiedViaPoint(:,2),simplifiedViaPoint(:,3));
axis equal; grid on;

figure; hold on;
for i = 1:length(stair)
    plot3(position(simplifiedViaIndex(stair(i)):simplifiedViaIndex(stair(i)+1),1),position(simplifiedViaIndex(stair(i)):simplifiedViaIndex(stair(i)+1),2),position(simplifiedViaIndex(stair(i)):simplifiedViaIndex(stair(i)+1),3));
end
axis equal; grid on;

end

