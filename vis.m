function [ simplifiedViaPoint, simplifiedViaIndex ] = vis( viaPoint, viaIndex, theta )
% This function is the third and last step of curve simplification, the
% function uses an idea similar to Visvalingam's algorithm, see: bost.ocks.org/mike/simplify/

% calculate the maximum angle of the triangles formed by consecutive points
angle = maxAngle(viaPoint(1:end-2,:),viaPoint(2:end-1,:),viaPoint(3:end,:));

% if the maximum angle is greater than a threshold, such as 2.85 radians,
% then we think the three points are close enough to a straight line, and
% just delete the middle points.
if max(angle) > theta
    viaPoint(find(angle > theta)+1,:) = [];
    viaIndex(find(angle > theta)+1) = [];
    % recursively check and delete
    [viaPoint,viaIndex] = vis(viaPoint,viaIndex,theta);
end
simplifiedViaPoint = viaPoint;
simplifiedViaIndex = viaIndex;

% plot
% figure; hold on;
% plot3(simplifiedViaPoint(:,1),simplifiedViaPoint(:,2),simplifiedViaPoint(:,3));
% axis equal; grid on;

end

%% 
function [angle] = maxAngle(P1, P2, P3)
% calculate the max angle of a triangle from three points, P can be matrix
% with each row represent a point

P1P2 = sqrt(sum((P1-P2).^2,2));
P2P3 = sqrt(sum((P2-P3).^2,2));
P1P3 = sqrt(sum((P1-P3).^2,2));

angle1 = acos((P1P2.^2+P1P3.^2-P2P3.^2)./(2.*P1P2.*P1P3));
angle2 = acos((P1P2.^2+P2P3.^2-P1P3.^2)./(2.*P1P2.*P2P3));
angle3 = acos((P1P3.^2+P2P3.^2-P1P2.^2)./(2.*P1P3.*P2P3));

angle = max([angle1,angle2,angle3],[],2);

end