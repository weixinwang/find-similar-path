function [ simplifiedViaPoint, simplifiedViaIndex ] = RDP( viaPoint, viaIndex, epsilon )
% This function is the second step of curve simplification, which uses the 
% Ramer�Douglas�Peucker algorithm, see en.wikipedia.org/wiki/Ramer�Douglas�Peucker_algorithm

% find max distance from points to the line specified by the first and end points
[distance,index] = maxDistance(viaPoint(1,:),viaPoint(end,:),viaPoint);

if distance <= epsilon
    % if the maximum distance is smaller than a threshold, such as 1m, then
    % only leave this point (and the start and end point) and delete all other points
    simplifiedViaPoint = viaPoint([1,index,size(viaPoint,1)],:);
    simplifiedViaIndex = [viaIndex(1),viaIndex(index),viaIndex(end)];
else
    % if not, recursively examine the two sub-curves divided by the further point
    [simplifiedViaPoint1,simplifiedViaIndex1] = RDP(viaPoint(1:index,:),viaIndex(1:index),epsilon);
    [simplifiedViaPoint2,simplifiedViaIndex2] = RDP(viaPoint(index:end,:),viaIndex(index:end),epsilon);
    simplifiedViaPoint = [simplifiedViaPoint1;simplifiedViaPoint2(2:end,:)];
    simplifiedViaIndex = [simplifiedViaIndex1,simplifiedViaIndex2(2:end)];
end

end

%%
function [ distance, index ] = maxDistance( lineStart, lineEnd, points )
% This function calculates the maximum distance from a cluster of points to
% a line specified by start and end points

v = (lineEnd-lineStart)/norm(lineEnd-lineStart);

[distance,index] = max(sqrt(sum(((lineStart-points)-((lineStart-points)*v')*v).^2,2)));

end

