function [ similarIndex1, similarIndex2 ] = findStraight( gyro1, acce1, position1, gyro2, acce2, position2 )
% This function tries to find out the straight line walking that happened
% at the same (close) locations. The data structure of similarIndex1 and
% similarIndex2 can be found in the comments on line 92.

% threshold
distanceThreshold = 15;        % minimum length of straight walking
RDPThreshold = 1;              % trajectory simplification parameters
VISThreshold = 2.85;
diffThreshold = 0.2;           % maximum distance between two paths in one trial
diffThreshold2 = 0.2;          % maximum distance between two paths in two trials
densityNumThreshold = 1;       % least connected number to be grouped into a cluster

% put the 1 and 2 together
n1 = length(gyro1);
n2 = length(gyro2);
gyro = [gyro1;gyro2]; clear gyro1 gyro2
acce = [acce1;acce2]; clear acce1 acce2
position = [position1;position2]; clear position1 position2

% simplify trajectory
viaPoint = cell(n1+n2,1);
viaIndex = cell(n1+n2,1);
simplifiedViaPoint = cell(n1+n2,1);
simplifiedViaIndex = cell(n1+n2,1);
for i = 1:n1+n2
    % note: the consequence of this trajectory simplification is the
    % trajectory between simplifiedViaPoints is roughly straight
    [viaPoint{i},viaIndex{i}] = findViaPoint(gyro{i},acce{i},position{i});
    [simplifiedViaPoint{i},simplifiedViaIndex{i}] = RDP(viaPoint{i},viaIndex{i},RDPThreshold);
    [simplifiedViaPoint{i},simplifiedViaIndex{i}] = vis(simplifiedViaPoint{i},simplifiedViaIndex{i},VISThreshold);
end

% find straight walking by setting a distance threshold to the distance
% between two simplifiedViaPoints
straightStart = cell(n1+n2,1);
straightEnd = cell(n1+n2,1);
for i = 1:n1+n2
    distance = sqrt(sum(diff(simplifiedViaPoint{i}).^2,2));
    straightStart{i} = find(distance > distanceThreshold);
    straightEnd{i} = straightStart{i}+1;
end

% delete periods seem to be in vehicle
gyroMagThreshold = 1;
for i = 1:n1+n2
    aveGyroMag = zeros(length(straightStart{i}),1);
    for j = 1:length(straightStart{i})
        gyroMag = sqrt(sum(gyro{i}(simplifiedViaIndex{i}(straightStart{i}(j)):simplifiedViaIndex{i}(straightEnd{i}(j)),:).^2,2));
        aveGyroMag(j) = mean(gyroMag);
    end
    % if the IMU's motion is really small but is moving, we might treat it
    % as in a vehicle or in an elevator
    straightStart{i}(aveGyroMag<gyroMagThreshold) = [];
    straightEnd{i}(aveGyroMag<gyroMagThreshold) = [];
end

% calculate distance raster, i.e. the distance between any two straight lines
distance = cell(n1+n2);  % This is a n1+n2-by-n1+n2 cell matrix, only the top-right triangle is filled
for i = 1:n1+n2
    % distance in one trial
    
    % each cell is a n-by-n matrix, but only the top-right triangle is
    % filled with non-zeor numbers, n is the number of straight lines in
    % this trial
    distance{i,i} = zeros(length(straightStart{i}));
    for k = 1:length(straightStart{i})-1
        P1 = simplifiedViaPoint{i}(straightStart{i}(k),:);
        Q1 = simplifiedViaPoint{i}(straightEnd{i}(k),:);
        P2 = simplifiedViaPoint{i}(straightStart{i}(k+1:end),:);
        Q2 = simplifiedViaPoint{i}(straightEnd{i}(k+1:end),:);
        distance{i,i}(k,k+1:length(straightStart{i})) = lineDistance(P1,Q1,P2,Q2);
    end
    % distance between two trials
    for j = i+1:n1+n2
        % each cell is a n-by-m matrix, where n and m are the number of
        % straight lines in the two trial
        distance{i,j} = zeros(length(straightStart{i}),length(straightStart{j}));
        for k = 1:length(straightStart{i})
            P1 = simplifiedViaPoint{i}(straightStart{i}(k),:);
            Q1 = simplifiedViaPoint{i}(straightEnd{i}(k),:);
            P2 = simplifiedViaPoint{j}(straightStart{j},:);
            Q2 = simplifiedViaPoint{j}(straightEnd{j},:);
            distance{i,j}(k,:) = lineDistance(P1,Q1,P2,Q2);
        end
    end 
end

% group the straight lines happened at the same (close) locations into groups
groups = findDensityGroup(distance,diffThreshold,diffThreshold2,densityNumThreshold);

% each cell in similarIndex1 is a cell vector with length n, where n is the
% number of groups. Each cell in this cell vector is another cell vector
% with length m, where m is the number of lines in the session that is
% also in the group. Each cell in this another cell vector is the indices
% of the line in terms of the IMU index of the session. similarIndex2 has
% the same structure.
similarIndex1 = cell(n1,1);
similarIndex2 = cell(n2,1);
interIndex = [0,cumsum(cellfun(@(x)size(x,2),distance(1,:)))];
for i = 1:length(groups)
    for j = 1:n1
        trialIndex = find(groups{i}(:,2) == j);
        similarIndex1{j}{i} = cell(length(trialIndex),1);
        for k = 1:length(trialIndex)
            start = straightStart{j}(groups{i}(trialIndex(k),1)-interIndex(j));
            endd = straightEnd{j}(groups{i}(trialIndex(k),1)-interIndex(j));
            similarIndex1{j}{i}{k} = simplifiedViaIndex{j}(start):simplifiedViaIndex{j}(endd);
        end
    end
    
    for j = 1:n2
        trialIndex = find(groups{i}(:,2) == j+n1);
        similarIndex2{j}{i} = cell(length(trialIndex),1);
        for k = 1:length(trialIndex)
            start = straightStart{j+n1}(groups{i}(trialIndex(k),1)-interIndex(j+n1));
            endd = straightEnd{j+n1}(groups{i}(trialIndex(k),1)-interIndex(j+n1));
            similarIndex2{j}{i}{k} = simplifiedViaIndex{j+n1}(start):simplifiedViaIndex{j+n1}(endd);
        end
    end
end

% plot
figure; hold on;
for i = 1:length(similarIndex1{1})
    rng(i*10);
    color = rand(1,3);
    for j = 1:n1
        for k = 1:length(similarIndex1{j}{i})
            plot3(position{j}(similarIndex1{j}{i}{k},1),position{j}(similarIndex1{j}{i}{k},2),position{j}(similarIndex1{j}{i}{k},3),'Color',color);
        end
    end
    for j = 1:n2
        for k = 1:length(similarIndex2{j}{i})
            plot3(position{j+n1}(similarIndex2{j}{i}{k},1),position{j+n1}(similarIndex2{j}{i}{k},2),position{j+n1}(similarIndex2{j}{i}{k},3),'Color',color,'LineStyle','--');
        end
    end
end
axis equal; grid on;

end

%% line distance
function [distance] = lineDistance(P1, Q1, P2, Q2)
% line distance is the average of three different distance: vertical
% distance, parallel distance and angle distance, normalized by the length
% of the shorter line

% P1, Q1 form segment1; P2, Q2 form segment2. P1, Q1 must be two points;
% P2, Q2 can be two n-by-3 matrices

% find out which line is shorter
length1 = norm(P1-Q1);
length2 = sqrt(sum((P2-Q2).^2,2));
PShort(length1 < length2,:) = P1.*ones(length(find(length1 < length2)),3);
PShort(length1 >= length2,:) = P2(length1 >= length2,:);
QShort(length1 < length2,:) = Q1.*ones(length(find(length1 < length2)),3);
QShort(length1 >= length2,:) = Q2(length1 >= length2,:);
PLong(length1 >= length2,:) = P1.*ones(length(find(length1 >= length2)),3);
PLong(length1 < length2,:) = P2(length1 < length2,:);
QLong(length1 >= length2,:) = Q1.*ones(length(find(length1 >= length2)),3);
QLong(length1 < length2,:) = Q2(length1 < length2,:);

% vertical distance is the sum of the distance of the two end points of the
% shorter line to the longer line
dv1 = verticalDistance(PShort,PLong,QLong);
dv2 = verticalDistance(QShort,PLong,QLong);
dv = dv1+dv2;

% parallel distance
dp = parallelDistance(PShort,QShort,PLong,QLong);

% angle distance
da = angleDistance(PShort,QShort,PLong,QLong);

distance = (dv+dp+da)/3./sqrt(sum((PShort-QShort).^2,2));

end


function [distance] = verticalDistance(P,lineStart,lineEnd)
% vertical distance is the distance from P to the line

n = (lineEnd-lineStart)./sqrt(sum((lineEnd-lineStart).^2,2));
v = (lineStart-P)-sum((lineStart-P).*n,2).*n;
distance = sqrt(sum(v.^2,2));

end


function [distance] = parallelDistance(P,Q,lineStart,lineEnd)
% parallelDistance is the sum of distance between the perpendicular feet of
% P, Q on the line to the starting points of the line.
% this parallelDistance seems to incorporate the direction of the two lines
% because the sequence of P and Q matters.

% find the two perpendicular feet
n = (lineEnd-lineStart)./sqrt(sum((lineEnd-lineStart).^2,2));
Pf = lineStart-sum((lineStart-P).*n,2).*n;
Qf = lineStart-sum((lineStart-Q).*n,2).*n;

% decide whether the foot point is on the segment or not
PJ = sum((Pf-lineStart).*(Pf-lineEnd),2);
QJ = sum((Qf-lineStart).*(Qf-lineEnd),2);

% distance
PfP2 = sqrt(sum((Pf-lineStart).^2,2));
% dP(PJ > 1e-10) = PfP2(PJ > 1e-10);
% dP(PJ <= 1e-10) = 0;
dP = PfP2';

QfQ2 = sqrt(sum((Qf-lineEnd).^2,2));
% dQ(QJ > 1e-10) = QfQ2(QJ > 1e-10);
% dQ(QJ <= 1e-10) = 0;
dQ = QfQ2';

distance = (dP+dQ)';
    
end


function [distance] = angleDistance(P,Q,lineStart,lineEnd)
% angleDistance is the angle between the two lines times the length of the
% short line. Angle distance incorporates the directions because if the two
% line are in opposite direction, acos will give pi, not 0.

theta = acos(sum((P-Q).*(lineStart-lineEnd),2)./(sqrt(sum((P-Q).^2,2)).*sqrt(sum((lineStart-lineEnd).^2,2))));
distance = theta.*sqrt(sum((P-Q).^2,2));

end

%% find connected paths
function [groups] = findDensityGroup(distance, distanceThreshold1, distanceThreshold2, densityNumThreshold)
% This function deals with two conditions, for example, wearing sporting
% shoes and sandles, and returns the straight paths that happened at the 
% same (close) locations.

% fill the empty blocks of the distance raster using symmetry
for i = 1:length(distance)
    distance{i,i} = distance{i,i}+distance{i,i}';
    distance(i+1:end,i) = cellfun(@(x)x',distance(i,i+1:end),'UniformOutput',false);
end

% find all connections between all of the straight lines, connection is
% defined as the two lines have distance smaller than threshold1 if they
% are in the same session, or smaller than threshold2 if they are in
% different sessions.
connections = cell(sum(cellfun(@(x)size(x,2),distance(1,:))),1); % the indices of all lines connected to the i'th line is stored in the i'th cell
interIndex = [0,cumsum(cellfun(@(x)size(x,2),distance(1,:)))];
for i = 1:length(distance)
    for j = 1:length(distance{i,i})
        selfConnection = find(distance{i,i}(j,:)>0 & distance{i,i}(j,:)<distanceThreshold1)+interIndex(i);
        interConnection = cell(length(distance),1);
        for k = [1:i-1,i+1:length(distance)]
            interConnection{k} = find(distance{i,k}(j,:)>0 & distance{i,k}(j,:)<distanceThreshold2)+interIndex(k);
        end
        connections{j+interIndex(i)} = [selfConnection,cat(2,interConnection{~cellfun('isempty',interConnection)})];
    end
end

% group the lines happened at the same (close) locations. A group is
% defined as every line in the group has a connected line in the group
groups = [];
for i = 1:length(connections)
    % if the line is already in a group, check the next one
    alreadyin = false;
    for j = 1:length(groups)
        if any(groups{j} == i)
            alreadyin = true;
            break;
        end
    end
    
    % find the connection tree
    if ~alreadyin
        if ~isempty(connections{i})
            group = [];
            group = findConnection(group,i,connections);
            group = sort(group);
            groups{end+1} = group;
        end
    end
end

% delete non-density connected lines from group. Density connected is a
% concept in graph theory, here a line is defined as density connected if
% it is connected to at least NumThreshold other lines in the group
for i = 1:length(groups)
    while true
        n = length(groups{i});
        j = 1;
        while j <= length(groups{i})
            if length(intersect(connections{groups{i}(j)},groups{i})) < densityNumThreshold
                groups{i}(j) = [];
            else
                j = j+1;
            end
        end
        if length(groups{i}) == n || isempty(groups{i})
            break;
        end
    end
end
groups(cellfun(@isempty,groups)) = [];

% categorize each straight walking into different trials. groups is a cell
% array, each cell is a n-by-2 matrix where n is the number of lines if the
% group. The first column of the array is the index of the line out of all
% lines, the second column of the array is the index of the session where
% the line is.
for i = 1:length(groups)
    index = cell(length(distance),1);
    for j = 1:length(distance)
        index{j} = find(groups{i}>interIndex(j) & groups{i}<=interIndex(j+1));
        groups{i}(index{j},2) = j;
    end
end

end


function [group] = findConnection(group,n,connection)
% This function is iterated recursively to exhaust the connection tree

for i = 1:length(connection{n})
    if ~any(group == connection{n}(i))
        group = [group;connection{n}(i)];
        group = findConnection(group,connection{n}(i),connection);
    end
end

end

