function [  ] = staAnalysis(  )
% The function read gait parameters from "parameters data" folder in the
% parent dir. If there's no such folder or the data in the folder is not
% formalized, an error will return.
% All linear regression figures will be stored in "linear regression 
% figure" folder in the parent dir. If there's no such folder, a new folder
% will be created; if the folder is not empty, this function will first
% delete all the exsiting contents in the folder.
% 
% The function does statistical analysis on gait parameters
% last edited by Weixin Wang, January 5, 2019

addpath('figure properties');

% check folder
pathNameData = '../parameters data';
pathNameFig = '../linear regression figure';
if ~exist(pathNameData,'dir')
    error('No \"parameters data\" folder in parent dir');
end
if ~exist(pathNameFig,'dir')
    mkdir(pathNameFig);
end

% count number of paths
list = dir(pathNameData);
numOfPath = 0;
if size(list,1) == 2
    error('no data in \"parameters data\" folder');
end
for i = 3:size(list,1)
    fileName = list(i).name;
    if strcmp(fileName(1:4),'path')
        numOfPath = numOfPath+1;
    end
end
if numOfPath == 0
    error('no data in \"parameters data\" folder');
end

% read data
data = cell(numOfPath,1);
for i = 1:numOfPath
    fileName = strcat(pathNameData,'/path',num2str(i),'.mat');
    if ~exist(fileName,'file')
        error('data in \"parameters data\" are not names consistently');
    end
    data{i} = load(fileName);
end

% check if "linear regression figure" is empty
list = dir(pathNameFig);
if size(list,1) > 2
    for i = 3:size(list,1)
        fileName = strcat(pathNameFig,'/',list(i).name);
        delete(fileName);
    end
end

% figure defaults
color = [0    0.4470    0.7410
    0.8500    0.3250    0.0980
    0.9290    0.6940    0.1250
    0.4940    0.1840    0.5560
    0.4660    0.6740    0.1880
    0.3010    0.7450    0.9330
    0.6350    0.0780    0.1840];

%% linear regression
% slopeSig is a matrix indicating whether a linear regression of some gait
% parameter versus velocity has a slope significantly different from zero,
% with 1 indicating significantly different, 0 otherwise. Each row is for a
% single path; 3 columns are arranged by stride length, stride width and
% virtual clearance. This will determine whether use t-test or ANCOVA.
slopeSig = zeros(numOfPath,3);
for i = 1:numOfPath
    % stride length
    x1 = cell2mat(cat(2,data{i}.velocity1{:})');
    y1 = cell2mat(cat(2,data{i}.strideLength1{:})');
    x2 = cell2mat(cat(2,data{i}.velocity2{:})');
    y2 = cell2mat(cat(2,data{i}.strideLength2{:})');
    mdl1 = fitlm(x1,y1);  % linear regression
    mdl2 = fitlm(x2,y2);
    slopeSig(i,1) = mdl1.Coefficients.pValue(2)<0.05 || mdl2.Coefficients.pValue(2)<0.05; % if either condition has a non-zero slope
    xtest = linspace(min(min(x1),min(x2)),max(max(x1),max(x2)),100)'; % this line and below is for plotting
    [yPre1,ci1] = predict(mdl1,xtest);
    [yPre2,ci2] = predict(mdl2,xtest);
    xmean = mean([x1;x2]);
    f = figure; hold on; setFigure(f,3,2.2);
    scatter(x1,y1,2,'filled');
    scatter(x2,y2,2,'filled');
    plot(xtest,yPre1,'Color','b');
    plot(xtest,yPre2,'Color','r');
    plot(xtest,ci1,'LineStyle',':','Color','b');
    plot(xtest,ci2,'LineStyle',':','Color','r');
    axes = gca; setAxes(axes,'Stride length [m]');
    ylim = axes.YAxis.Limits;
    plot([xmean;xmean],ylim,'Color','k','LineStyle','--');
    saveFigure(f,i,'strideLength',pathNameFig);
    
    % stride width
    y1 = cell2mat(cat(2,data{i}.strideWidth1{:})');
    y2 = cell2mat(cat(2,data{i}.strideWidth2{:})');
    mdl1 = fitlm(x1,y1);
    mdl2 = fitlm(x2,y2);
    slopeSig(i,2) = mdl1.Coefficients.pValue(2)<0.05 || mdl2.Coefficients.pValue(2)<0.05;
    xtest = linspace(min(min(x1),min(x2)),max(max(x1),max(x2)),100)';
    [yPre1,ci1] = predict(mdl1,xtest);
    [yPre2,ci2] = predict(mdl2,xtest);
    xmean = mean([x1;x2]);
    f = figure; hold on; setFigure(f,3,2.2);
    scatter(x1,y1,2,'filled');
    scatter(x2,y2,2,'filled');
    plot(xtest,yPre1,'Color','b');
    plot(xtest,yPre2,'Color','r');
    plot(xtest,ci1,'LineStyle',':','Color','b');
    plot(xtest,ci2,'LineStyle',':','Color','r');
    axes = gca; setAxes(axes,'Stride width [m]');
    ylim = axes.YAxis.Limits;
    plot([xmean;xmean],ylim,'Color','k','LineStyle','--');
    saveFigure(f,i,'strideWidth',pathNameFig);
    
    % clearance
    y1 = cell2mat(cat(2,data{i}.clearance1{:})');
    y2 = cell2mat(cat(2,data{i}.clearance2{:})');
    mdl1 = fitlm(x1,y1);
    mdl2 = fitlm(x2,y2);
    slopeSig(i,3) = mdl1.Coefficients.pValue(2)<0.05 || mdl2.Coefficients.pValue(2)<0.05;
    xtest = linspace(min(min(x1),min(x2)),max(max(x1),max(x2)),100)';
    [yPre1,ci1] = predict(mdl1,xtest);
    [yPre2,ci2] = predict(mdl2,xtest);
    xmean = mean([x1;x2]);
    f = figure; hold on; setFigure(f,3,2.2);
    scatter(x1,y1,2,'filled');
    scatter(x2,y2,2,'filled');
    plot(xtest,yPre1,'Color','b');
    plot(xtest,yPre2,'Color','r');
    plot(xtest,ci1,'LineStyle',':','Color','b');
    plot(xtest,ci2,'LineStyle',':','Color','r');
    axes = gca; setAxes(axes,'Clearance [m]');
    ylim = axes.YAxis.Limits;
    plot([xmean;xmean],ylim,'Color','k','LineStyle','--');
    saveFigure(f,i,'clearance',pathNameFig);
end
close all;

%% ancova or t-test
% sig is a matrix coding whether gait parameters are different for two
% different conditions. Coding sequence is the same as "slopeSig". If the
% corresponding slopeSig is 1, ANCOVA is used for hypothesis testing;
% otherwise two-sample t-test is used.
sig = zeros(numOfPath,3);
% mean and CI are mean and confidence interval (95%) for differenet gait
% parameters on different paths. Coding sequence is the same as "slopeSig".
% If the corresponding slopeSig is 1, both will be adjusted by velocity.
meanGait = cell(numOfPath,3);
CIGait = cell(numOfPath,3);
stdGait = cell(numOfPath,3);
for i = 1:numOfPath
    % stride length
    x = [cell2mat(cat(2,data{i}.velocity1{:})');cell2mat(cat(2,data{i}.velocity2{:})')];
    y1 = cell2mat(cat(2,data{i}.strideLength1{:})');
    y2 = cell2mat(cat(2,data{i}.strideLength2{:})');
    if slopeSig(i,1)
        % ANCOVA analysis
        group = [zeros(length(cell2mat(cat(2,data{i}.velocity1{:})')),1);ones(length(cell2mat(cat(2,data{i}.velocity2{:})')),1)];
        [h,atab,ctab,stats] = aoctool(x,[y1;y2],group,0.05,'','','','off');
        if atab{4,6} > 0.05
            [h,atab,ctab,stats] = aoctool(x,[y1;y2],group,0.05,'','','','off','parallel lines');
        end
        [c,m] = multcompare(stats,'Estimate','pmm','Display','off');
        sig(i,1) = atab{2,6};
        meanGait{i,1} = table(stats.pmm(1),stats.pmm(2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
        CIGait{i,1} = table(adjustedCI(y1,stats.pmm(1),m(1,2)),adjustedCI(y2,stats.pmm(2),m(2,2)),...
            'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
        stdGait{i,1} = table(m(1,2)*sqrt(length(y1)),m(2,2)*sqrt(length(y2)),...
            'VariableNames',{'sportshoes','sandals'},'RowNames',{'standard deviation'});
    else
        % two sample t-test
        [h,p] = ttest2(y1,y2);
        sig(i,1) = p;
        meanGait{i,1} = table(mean(y1),mean(y2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
        CIGait{i,1} = table(CI(y1),CI(y2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
        stdGait{i,1} = table(std(y1),std(y2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'standard deviation'});
    end
    
    % stride width
    y1 = cell2mat(cat(2,data{i}.strideWidth1{:})');
    y2 = cell2mat(cat(2,data{i}.strideWidth2{:})');
    if slopeSig(i,2)
        group = [zeros(length(cell2mat(cat(2,data{i}.velocity1{:})')),1);ones(length(cell2mat(cat(2,data{i}.velocity2{:})')),1)];
        [h,atab,ctab,stats] = aoctool(x,[y1;y2],group,0.05,'','','','off');
        if atab{4,6} > 0.05
            [h,atab,ctab,stats] = aoctool(x,[y1;y2],group,0.05,'','','','off','parallel lines');
        end
        [c,m] = multcompare(stats,'Estimate','pmm','Display','off');
        sig(i,2) = atab{2,6};
        meanGait{i,2} = table(stats.pmm(1),stats.pmm(2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
        CIGait{i,2} = table(adjustedCI(y1,stats.pmm(1),m(1,2)),adjustedCI(y2,stats.pmm(2),m(2,2)),...
            'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
        stdGait{i,2} = table(m(1,2)*sqrt(length(y1)),m(2,2)*sqrt(length(y2)),...
            'VariableNames',{'sportshoes','sandals'},'RowNames',{'standard deviation'});
    else
        [h,p] = ttest2(y1,y2);
        sig(i,2) = p;
        meanGait{i,2} = table(mean(y1),mean(y2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
        CIGait{i,2} = table(CI(y1),CI(y2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
        stdGait{i,2} = table(std(y1),std(y2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'standard deviation'});
    end
    
    % clearance
    y1 = cell2mat(cat(2,data{i}.clearance1{:})');
    y2 = cell2mat(cat(2,data{i}.clearance2{:})');
    if slopeSig(i,3)
        group = [zeros(length(cell2mat(cat(2,data{i}.velocity1{:})')),1);ones(length(cell2mat(cat(2,data{i}.velocity2{:})')),1)];
        [h,atab,ctab,stats] = aoctool(x,[y1;y2],group,0.05,'','','','off');
        if atab{4,6} > 0.05
            [h,atab,ctab,stats] = aoctool(x,[y1;y2],group,0.05,'','','','off','parallel lines');
        end
        [c,m] = multcompare(stats,'Estimate','pmm','Display','off');
        sig(i,3) = atab{2,6};
        meanGait{i,3} = table(stats.pmm(1),stats.pmm(2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
        CIGait{i,3} = table(adjustedCI(y1,stats.pmm(1),m(1,2)),adjustedCI(y2,stats.pmm(2),m(2,2)),...
            'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
        stdGait{i,3} = table(m(1,2)*sqrt(length(y1)),m(2,2)*sqrt(length(y2)),...
            'VariableNames',{'sportshoes','sandals'},'RowNames',{'standard deviation'});
    else
        [h,p] = ttest2(y1,y2);
        sig(i,3) = p;
        meanGait{i,3} = table(mean(y1),mean(y2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'mean'});
        CIGait{i,3} = table(CI(y1),CI(y2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'UpperBount','LowerBound'});
        stdGait{i,3} = table(std(y1),std(y2),'VariableNames',{'sportshoes','sandals'},'RowNames',{'standard deviation'});
    end
    
    % plot
    f = figure; setFigure(f,3.1,2.4);
    subplot(1,3,1); hold on;
    h = bar(1,meanGait{i,1}.sportshoes); h.FaceColor = color(1,:);
    errorbar(1,meanGait{i,1}.sportshoes,meanGait{i,1}.sportshoes-CIGait{i,1}.sportshoes(1),'Color','k');
    %errorbar(1,meanGait{i,1}.sportshoes,stdGait{i,1}.sportshoes(1),'Color','k');
    h = bar(2,meanGait{i,1}.sandals); h.FaceColor = color(2,:);
    errorbar(2,meanGait{i,1}.sandals,meanGait{i,1}.sandals-CIGait{i,1}.sandals(1),'Color','k');
    %errorbar(2,meanGait{i,1}.sandals,stdGait{i,1}.sandals(1),'Color','k');
    ax = gca; setAxes2(ax,1,sig(i,1));
    
    subplot(1,3,2); hold on;
    h = bar(1,meanGait{i,2}.sportshoes); h.FaceColor = color(1,:);
    errorbar(1,meanGait{i,2}.sportshoes,meanGait{i,2}.sportshoes-CIGait{i,2}.sportshoes(1),'Color','k');
    %errorbar(1,meanGait{i,2}.sportshoes,stdGait{i,2}.sportshoes(1),'Color','k');
    h = bar(2,meanGait{i,2}.sandals); h.FaceColor = color(2,:);
    errorbar(2,meanGait{i,2}.sandals,meanGait{i,2}.sandals-CIGait{i,2}.sandals(1),'Color','k');
    %errorbar(2,meanGait{i,2}.sandals,stdGait{i,2}.sandals(1),'Color','k');
    ax = gca; ax.XLim = [0,3]; ax.XTick = [];
    setAxes2(ax,2,sig(i,2));
    
    subplot(1,3,3); hold on;
    h = bar(1,meanGait{i,3}.sportshoes); h.FaceColor = color(1,:);
    errorbar(1,meanGait{i,3}.sportshoes,meanGait{i,3}.sportshoes-CIGait{i,3}.sportshoes(1),'Color','k');
    %errorbar(1,meanGait{i,3}.sportshoes,stdGait{i,3}.sportshoes(1),'Color','k');
    h = bar(2,meanGait{i,3}.sandals); h.FaceColor = color(2,:);
    errorbar(2,meanGait{i,3}.sandals,meanGait{i,3}.sandals-CIGait{i,3}.sandals(1),'Color','k');
    %errorbar(2,meanGait{i,3}.sandals,stdGait{i,3}.sandals(1),'Color','k');
    ax = gca; ax.XLim = [0,3]; ax.XTick = [];
    setAxes2(ax,3,sig(i,3));
    saveFigure(f,i,'comparison',pathNameFig);
end

rmpath('figure properties');

end

%% calculating confidence interval
function [interval] = CI(x,alpha)
% calculate confidence interval

if ~exist('alpha','var')
    alpha = 0.05;
end

sem = std(x)/sqrt(length(x));
ts = tinv(1-alpha/2,length(x)-1);
err = ts*sem;

interval = [mean(x)-err;mean(x)+err];

end

function [interval] = adjustedCI(x,adjustedMean,adjustedSE,alpha)
% calculate adjusted confidence interval

if ~exist('alpha','var')
    alpha = 0.05;
end

ts = tinv(1-alpha/2,length(x)-1);
err = ts*adjustedSE;

interval = [adjustedMean-err;adjustedMean+err];

end

